package UT4_Servicios.E08_Chat.tcp.Cliente;

import java.net.*;
import java.util.Calendar;
import java.util.Scanner;

class TCP2_Cliente {
	private Cliente_Escritor escritor;
	private Cliente_Escuchador escuchador;
	private Calendar horaServer = Calendar.getInstance();

	public TCP2_Cliente(Socket server) {
		escuchador = new Cliente_Escuchador(this, server);
		(new Thread(escuchador)).start();

		escritor = new Cliente_Escritor(this, server);
		(new Thread(escritor)).start();
	}

	public static void main(String args[]) throws Exception {
		Socket clientSocket = null;
		Scanner entrada = new Scanner(System.in);
		try {
			if (args.length > 1) {
				InetAddress direccion = InetAddress.getByName(args[0]);
				clientSocket = new Socket(direccion, Integer.parseInt(args[1]));
			} else {
				System.out.println("Introduzca la ip del servidor: ");
				String host = entrada.next();
				System.out.println("Introduzca el puerto del servidor: ");
				String puerto = entrada.next();
				InetAddress direccion = InetAddress.getByName(host);
				clientSocket = new Socket(direccion, Integer.parseInt(puerto));
			}
		} catch (ConnectException error) {
			System.err
					.println("No se ha podido conectar, error en la conexión.");
			System.exit(1);
		} catch (NumberFormatException error) {
			System.err
					.println("El puerto introducido no es válido, introduce únicamente números, error en la conexión.");
			System.exit(1);
		} catch (SocketException socketError) {
			System.err.println("error al construir el socket");
			System.exit(1);
		}
		new TCP2_Cliente(clientSocket);

	}
	

	public Calendar getHoraServer() {
		return horaServer;
	}
}