package UT4_Servicios.udp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;

public class Servidor_Escritor implements Runnable {
	private UDP_Servidor padre;

	public Servidor_Escritor(UDP_Servidor padre) {
		this.padre = padre;
	}

	// escribe a todos

	@Override
	public void run() {
		try {
			BufferedReader entradaDeTeclado = new BufferedReader(
					new InputStreamReader(System.in));

			while (true) {
				
				System.out.println("Introduce el mensaje que quieres enviar :");
				String mensaje = (String) entradaDeTeclado.readLine();
				byte[] bmensaje = mensaje.getBytes();

				DatagramPacket paqueteConMensaje = new DatagramPacket(bmensaje,
						bmensaje.length, padre.getCliente().getAddress(),
						padre.getCliente().getPort());
				padre.getSocket().send(paqueteConMensaje);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
