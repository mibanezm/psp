package UT2_Multihilo;

import UT0_Repaso.CCC;
import UT0_Repaso.Fecha;

/* Cajeros
 * Esta clase representa a cajeros de banco que acceden de forma
 * concurrente a cuentas bancarias de acceso sincronizado por bloques
 * El programa de prueba creará múltiples hilos cajeros que meten y sacan dinero
 * concurrentemente, debiendo ser al final el balance correcto
 */

public class P02_CajeroSC implements Runnable {
	private double dinero;
	private P02_CuentaBancariaSC cuenta;

	public void run() {
		if (dinero>=0)
			cuenta.ingresar(dinero);
		else
			try {
				cuenta.reintegrar(-dinero);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
	}

	P02_CajeroSC(P02_CuentaBancariaSC cuenta,double dinero) {
		this.dinero=dinero;
		this.cuenta=cuenta;
	}


	public static void main(String args []) {
		P02_CuentaBancariaSC c=null;
		
		int numOperaciones=2000;
		double saldoInicial=1000;
		
		Thread ti[]=new Thread[numOperaciones],
			   tr[]=new Thread[numOperaciones];
		
		try {
			c= new P02_CuentaBancariaSC(
					new CCC("2038.4712.  .3111483575"),
					new Fecha(),
					saldoInicial );
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		//Crear los hilos que meten y sacan dinero
		for (int i=0; i<numOperaciones; i++)
		{
			ti[i]=new Thread(new P02_CajeroSC(c,1.));	//cada P02_CajeroSC mete 1 euro
			tr[i]=new Thread(new P02_CajeroSC(c,-1.));	//cada P02_CajeroSC saca 1 euro
			ti[i].start();
			tr[i].start();
		}
		
		//Esperar que terminen los hilos
		for (int i=0; i<numOperaciones; i++)
		{
			try {
				ti[i].join();
				tr[i].join();
			} catch (InterruptedException e) {}
		}
		
		System.out.println("El saldo debería ser "+saldoInicial);
		System.out.println("saldo="+c.getSaldo());
	}
}
