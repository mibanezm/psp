package UT3_Comunicaciones;

import java.io.*;
import java.net.*;
import java.util.*;

public class E06_HiloServidorCitas extends Thread {
	 
	    protected DatagramSocket socket = null;
	    protected BufferedReader in = null;
	    protected boolean masCitas = true;
	 
	    public E06_HiloServidorCitas() throws IOException {
	    this("E06_HiloServidorCitas");
	    }
	 
	    public E06_HiloServidorCitas(String name) throws IOException {
	        super(name);
	        socket = new DatagramSocket(4445);
	 
	        try {
	            in = new BufferedReader(new FileReader("fichero-lineas-citas.txt"));
	        } catch (FileNotFoundException e) {
	            System.err.println("No se pudo abrir fichero de citas. Sirviendo la hora en su lugar.");
	        }
	    }
	 
	    public void run() {
	 
	        while (masCitas) {
	            try {
	                byte[] buf = new byte[256];
	 
	                // recibir solicitud
	                DatagramPacket packet = new DatagramPacket(buf, buf.length);
	                socket.receive(packet);
	 
	                // preparar respuesta
	                String dString = null;
	                if (in == null)
	                    dString = new Date().toString();
	                else
	                    dString = obtenerSiguienteCita();
	 
	                buf = dString.getBytes();
	 
	        // enviar la respuesta
	                InetAddress address = packet.getAddress();
	                int port = packet.getPort();
	                packet = new DatagramPacket(buf, buf.length, address, port);
	                socket.send(packet);
	            } catch (IOException e) {
	                e.printStackTrace();
	        masCitas = false;
	            }
	        }
	        socket.close();
	    }
	 
	    protected String obtenerSiguienteCita() {
	        String retorno = null;
	        try {
	            if ((retorno = in.readLine()) == null) {
	                in.close();
	        masCitas = false;
	                retorno = "No hay más citas. Adiós.";
	            }
	        } catch (IOException e) {
	            retorno = "IOException en servidor.";
	        }
	        return retorno;
	    }
	}