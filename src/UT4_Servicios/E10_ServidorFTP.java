package UT4_Servicios;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class E10_ServidorFTP {
	private ServerSocket socketServidor;
	private int puerto;
	private final String directorio;

	public E10_ServidorFTP(int puerto, String directorio) {
		this.puerto = puerto;
		this.directorio = directorio;
	}

	public class Cliente extends Thread {
		private Socket control;
		private Socket datos;
		private DataOutputStream out;
		private BufferedReader in;
		private String mensaje;
		private File directorioActual;

		public Cliente(Socket s) {
			this.control = s;
			directorioActual = new File(directorio);
		}

		@Override
		public void run() {
			try {
				System.out.printf("Conexión recibida desde %s:%d\n", control
						.getInetAddress().getHostName(), control.getPort());
				try {
					out = new DataOutputStream(control.getOutputStream());
				} catch (IOException ioe) {
					System.err.println("Erron en la salida hacia el cliente");
					throw new IOException();
				}
				try {
					in = new BufferedReader(new InputStreamReader(
							control.getInputStream()));
				} catch (IOException ioe) {
					System.err.println("Erron en la entrada desde el cliente");
					throw new IOException();
				}

				enviarMensaje("220 Bienvenido al servidor FTP privado de Daniel");
				recibirMensaje();
				while (!mensaje.startsWith("USER ")) {
					enviarMensaje("530 Not logged in");
					recibirMensaje();
				}
				enviarMensaje("331 User name okay, need password");
				recibirMensaje();
				while (!mensaje.startsWith("PASS ")) {
					enviarMensaje("530 Not logged in");
					recibirMensaje();
				}
				enviarMensaje("230 User logged in, proceed");
				recibirMensaje();
				while (!mensaje.equals("SYST")) {
					enviarMensaje("503 Bad sequence of commands");
					recibirMensaje();
				}
				enviarMensaje("215 UNIX Type: L8");
				recibirMensaje();
				while (!mensaje.startsWith("TYPE ")) {
					enviarMensaje("503 Bad sequence of commands");
					recibirMensaje();
				}
				enviarMensaje("200 TYPE is now 8-bit binary");

				do {
					recibirMensaje();
					if (mensaje.equals("PWD")) {
						enviarMensaje(String.format(
								"257 \"%s%s\" is your current location",
								directorioActual.getPath().substring(
										directorio.length()), "/"));
					} else if (mensaje.startsWith("CWD")) {
						String ruta = mensaje.substring(4);
						File f = new File(directorio + ruta);
						if (f.exists()) {
							directorioActual = f;
							enviarMensaje(String.format(
									"250 OK. Current directory is %s", ruta));
						} else {
							enviarMensaje(String
									.format("550 Can't change directory to %s: No such file or directory",
											ruta));
						}
					} else if (mensaje.equals("PASV")) {
						byte[] ip = InetAddress.getLocalHost().getAddress();
						ServerSocket servidorDatos = new ServerSocket(0);
						int puertoDatos = servidorDatos.getLocalPort();
						enviarMensaje(String
								.format("227 Entering Passive Mode (%d,%d,%d,%d,%d,%d)",
										ip[0] & 0xff, ip[1] & 0xff,
										ip[2] & 0xff, ip[3] & 0xff,
										(puertoDatos & 0xff00) >>> 8,
										puertoDatos & 0xff));
						datos = servidorDatos.accept();
						recibirMensaje();
						enviarMensaje("150 Accepted data connection");
						if (mensaje.startsWith("LIST ")) {
							enviarDatos(getListado());
						} else if (mensaje.startsWith("RETR ")) {
							String ruta = mensaje.substring(5);
							File f = new File(directorio + ruta);
							enviarFichero(f);
						} else if (mensaje.startsWith("STOR ")) {
							String ruta = mensaje.substring(5);
							File f = new File(directorio + ruta);
							recibirFichero(f);
						} else {
							enviarMensaje("502 Command not implemented");
						}
					} else {
						enviarMensaje("502 Command not implemented");
					}
				} while (!mensaje.equals("QUIT"));
			} catch (IOException ioException) {
			} catch (NullPointerException npe) {
			} finally {
				try {
					if (control != null) {
						in.close();
						out.close();
						control.close();
					}
					System.out.printf("Desconectado %s:%d\n", control
							.getInetAddress().getHostName(), control.getPort());
				} catch (IOException ioException) {
				}
			}

		}

		public void enviarMensaje(String msg) {
			try {
				byte b[] = (msg + "\r\n").getBytes();
				out.write(b);
				System.out
						.printf("servidor hacia %s:%d>%s\n", control
								.getInetAddress().getHostName(), control
								.getPort(), msg);
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}

		public void recibirMensaje() throws IOException {
			mensaje = in.readLine();
			if (mensaje != null) {
				System.out.printf("cliente desde %s:%d>%s\n", control
						.getInetAddress().getHostAddress(), control.getPort(),
						mensaje);
			}
		}

		/**
		 * Ejecuta en linea de comandos ls -l y guarda el resultado en un String
		 * 
		 * @return el listado de ficheros del directorio actual del cliente en
		 *         el servidor
		 */
		public String getListado() {
			StringBuilder builder = new StringBuilder();
			try {
				Runtime r = Runtime.getRuntime();
				Process p = r.exec(String.format("ls -l %s%s",
						directorioActual.getPath(), "/"));
				BufferedReader bri = new BufferedReader(new InputStreamReader(
						p.getInputStream()));
				String linea;
				while ((linea = bri.readLine()) != null) {
					builder.append(linea);
					builder.append("\r\n");
				}
				bri.close();
				p.waitFor();
			} catch (Exception e) {
			}
			return builder.toString();
		}

		/**
		 * Envia un String al cliente por el socket de datos
		 * 
		 * @param s
		 *            la cadena
		 */
		public void enviarDatos(String s) {
			DataOutputStream out = null;
			try {
				out = new DataOutputStream(datos.getOutputStream());
				byte[] b = s.getBytes();
				out.write(b);
				enviarMensaje("226 Transfer succesfull");
			} catch (IOException e) {
			} finally {
				try {
					out.close();
					datos.close();
				} catch (IOException e) {
				}
			}
		}

		public void enviarFichero(File f) {
			FileInputStream fis = null;
			DataOutputStream out = null;
			try {
				out = new DataOutputStream(datos.getOutputStream());
				fis = new FileInputStream(f);
				byte[] b = new byte[2048];
				int tam;
				while ((tam = fis.read(b)) > 0) {
					out.write(b, 0, tam);
				}
				enviarMensaje("226 File successfully transferred");
			} catch (IOException e) {
			} finally {
				try {
					fis.close();
					out.close();
					datos.close();
				} catch (IOException e) {
				}
			}
		}

		public void recibirFichero(File f) {
			DataInputStream in = null;
			FileOutputStream fos = null;
			try {
				in = new DataInputStream(datos.getInputStream());
				fos = new FileOutputStream(f);
				byte[] b = new byte[2048];
				int tam;
				while ((tam = in.read(b)) > 0) {
					fos.write(b, 0, tam);
				}
				enviarMensaje("226 File successfully received");
			} catch (IOException e) {
			} finally {
				try {
					fos.close();
					in.close();
					datos.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public void run() {
		try {
			socketServidor = new ServerSocket(puerto, 10);
			while (true) {
				try {
					System.out.println("Esperando conexión entrante");
					new Cliente(socketServidor.accept()).start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			System.err.printf(
					"No se puede crear el servidor en el puerto %d\n", puerto);
			System.exit(1);
		}

	}

	public static void main(String args[]) {
		try {
			E10_ServidorFTP servidor = new E10_ServidorFTP(Integer.parseInt(args[0]),
					args[1]);
			servidor.run();
		} catch (NumberFormatException nfe) {
			System.err
					.println("el primer argumento tienen que ser un número entero");
			System.exit(1);
		} catch (IndexOutOfBoundsException ioobe) {
			System.err
					.println("hay que introducir el puerto y el directorio ftp como argumentos");
			System.exit(1);
		}
	}
}
