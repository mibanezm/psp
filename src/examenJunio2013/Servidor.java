package examenJunio2013;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Concepto: programa servidor multihilo, que realiza un peculiar servicio de eco.
 * Autor: Miguel Ángel Ibáñez
 * Módulo: PSP (Programación de servicios y procesos)
 * El puerto de servicio (1107) se recibe desde la linea de argumentos o desde el entorno de ejecución/depuración de Eclipse
 * 
 * El hilo principal permanece en un bucle infinito que queda suspendido en la llamada bloqueante accept(), hasta que entra
 * 		una nueva conexión, se crea un hilo para esa conexión y se ejecuta una nueva iteración, quedando bloqueado de nuevo
 * 
 * Los hilos secundarios atienden una conexión cada uno. Cuando el cliente cierra la conexión ((bye), el hilo secundario termina.
 * 
 * La comunicación se hace mediante readObject() y writeObject()
 * 
 * El hilo secundario discrimina qué comando le llega del cliente mediante regex. Los comandos previstos son:
 * 		bye: el cliente desea cerrar la conexión, terminar el hilo que le atiende
 * 		[nº] mensaje: el cliente desea que le hagan eco de "mensaje" "n" veces (por defecto es 1)
*/


public class Servidor {

	static final String S="Servidor: ";	//Para imprimir antes de cada mensaje 

	private ServerSocket socketServidor;	//Socket de servicio, fijado por run()
	private int puerto;					//puerto de servicio, fijado por constructor

	public Servidor(int puerto) {
		this.puerto = puerto;
	}

	//Esta subclase se usa para crear un hilo receptor por conexión
	//Nota: una clase estática no necesita de una instancia de la clase exterior
	//      para poder ser creada
	public static class ClienteProcesar extends Thread {
		private Socket s;
		private ObjectOutputStream out;
		private ObjectInputStream in;
		private String mensaje;

		public ClienteProcesar(Socket s) {
			this.s = s;
		}

		public void run() {
			this.setName(S+"Hilo ClienteProcesar");
			try {
				System.out.printf(S+"Conexión recibida [%s;%d]\n", s
						.getInetAddress().getHostName(), s.getPort());

				//Crear los flujos de E/S necesarios para comunicar con el servidor
				try {
					out = new ObjectOutputStream(s.getOutputStream());
					out.flush();
				} catch (IOException ioe) {
					System.err.println(S+"Error E/S (creando out)");
					throw new IOException();
				}
				try {
					in = new ObjectInputStream(s.getInputStream());
				} catch (IOException ioe) {
					System.err.println(S+"Error E/S (creando in)");
					throw new IOException();
				}

				//Secuencia de digitos opcional, espacio opcional, mensaje
				Pattern msg=Pattern.compile("(\\d*)?[ ]?(.*)");
				Matcher matcher=null;
				
				//Leer comando desde el cliente
				do {
					try {
						mensaje = (String) in.readObject();
						System.out.printf(S+"mensaje desde [%s;%d]=%s\n", s
								.getInetAddress().getHostName(), s.getPort(),
								mensaje);
						if (mensaje.equals("bye"))
							enviarMensaje("bye");
						//Procesar otros mensajes
						else {
							matcher = msg.matcher(mensaje);
							if (matcher.matches()) {
								int n;
								try {
									n=Integer.parseInt(matcher.group(1));	//separar el número
								} catch (NumberFormatException e) {
									n=1;				//valor por defecto
								}
								String m=matcher.group(2);					//separar el mensaje
								for (int i=1; i<=n; i++) {		//enviar el mensaje "número" veces
									enviarMensaje(m);
								}
							}
						}
					} catch (ClassNotFoundException classNot) {
						System.err.println(S+"Datos recibidos en formato desconocido");
					}
				} while (!mensaje.equals("bye"));
			} catch (IOException ioException) {
				ioException.printStackTrace();
			} finally {
				//cerrar flujos y conexión
				try {
					if (s != null) {
						in.close();
						out.close();
						s.close();
					}
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
			}
		}

		void enviarMensaje(String msg) {
			try {
				out.writeObject(msg);
				out.flush();
				System.out.printf(S+"mensaje hacia [%s;%d]=%s\n", s
						.getInetAddress().getHostName(), s.getPort(), msg);
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	public void ejecutar() {
		try {
			socketServidor = new ServerSocket(puerto,10);
			while (true) {
				try {
					System.out.println(S+"Esperando conexión entrante");
					//accept() es bloqueante, hasta que entar una conexión, y en
					//	ese momento crea un objeto "Cliente" que controla la
					//  comunicación con el mismo
					new ClienteProcesar(socketServidor.accept()).start();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			System.err.printf(S+"No se puede crear el servidor en el puerto %d\n", puerto);
			System.exit(1);
		}
	}

	/**
	 * Recibe por linea de comandos el puerto de servicio
	 * en el ejercicio usar 1107/tcp
	 * @param  		args[0]: número de puerto
	 * @return     void
	 * @see         
	 */	
	public static void main(String args[]) {
		try {
			Servidor servidor = new Servidor(Integer.parseInt(args[0]));
			servidor.ejecutar();
		} catch (NumberFormatException e) {
			System.err.println(S+"El primer argumento tiene que ser un número entero");
			System.exit(1);
		} catch (IndexOutOfBoundsException e) {
			System.err.println(S+"Hay que introducir el puerto como argumento");
			System.exit(1);
		}
	}
}
