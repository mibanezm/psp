package UT4_Servicios.udp.server;

import java.io.IOException;
import java.net.InetAddress;

public class Servidor_Escuchador implements Runnable {
	private InetAddress cliente;
	private UDP_Servidor padre;
	private String nombre = "";

	public Servidor_Escuchador(UDP_Servidor padre) {
		this.padre = padre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public InetAddress getDireccion() {
		return cliente;
	}

	@Override
	public void run() {
		try {// communication loop
			while (true) {
				padre.getSocket().receive(padre.getCliente());
				byte[] data = padre.getCliente().getData();
				String mensaje = new String(data, 0, padre.getCliente().getLength());

				System.out.println(padre.getCliente().getAddress()
						.getHostAddress()
						+ " : " + padre.getCliente().getPort() + " - " + mensaje);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
