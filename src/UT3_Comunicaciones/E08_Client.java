package UT3_Comunicaciones;

import java.io.*;
import java.net.*;

public class E08_Client {
	Socket socketServicio;
	ObjectOutputStream out;
	ObjectInputStream in;
	String mensaje;
	E08_Client(){}
	void run()
	{
		try{
			socketServicio = new Socket("localhost", 1210);
			System.out.println("Conectado a localhost, puerto 1210");
			out = new ObjectOutputStream(socketServicio.getOutputStream());
			out.flush();
			in = new ObjectInputStream(socketServicio.getInputStream());
			do{
				try{
					mensaje = (String)in.readObject();
					System.out.println("servidor>" + mensaje);
					enviarMensaje("Hola, servidor");
					mensaje = "adiós";
					enviarMensaje(mensaje);
				}
				catch(ClassNotFoundException classNot){
					System.err.println("Datos recibidos en formato desconocido");
				}
			}while(!mensaje.equals("adiós"));
		}
		catch(UnknownHostException unknownHost){
			System.err.println("¡Intentando conectar a un host desconocido!");
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
		finally {
			try{
				in.close();
				out.close();
				socketServicio.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
	}
	void enviarMensaje(String msg)
	{
		try{
			out.writeObject(msg);
			out.flush();
			System.out.println("cliente>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	public static void main(String args[])
	{
		E08_Client cliente = new E08_Client();
		cliente.run();
	}
}