package UT3_Comunicaciones;

import java.io.*;
import java.net.*;
 
public class E06_ClienteCitas {

    public static void main(String[] args) throws IOException {
 
        if (args.length != 1) {
             System.out.println("Uso: java E06_ClienteCitas <servidor>");
             return;
        }
 
        // Preparar un socket UDP
        DatagramSocket socket = new DatagramSocket();
 
        // enviar petición
        byte[] buf = new byte[256];
        InetAddress address = InetAddress.getByName(args[0]);
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 4445);
        socket.send(packet);
     
        // recoger respuesta
        packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);
 
        // mostrar respuesta
        String recibido = new String(packet.getData(), 0, packet.getLength());
        System.out.println("Quote of the Moment: " + recibido);
     
        socket.close();
    }
}
