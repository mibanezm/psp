package UT4_Servicios.E08_Chat.tcp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Servidor_Escritor implements Runnable {
	private TCP3_Servidor padre;

	public Servidor_Escritor(TCP3_Servidor padre) {
		this.padre = padre;
	}

	// escribe a todos
	public void escribe(String mensaje) throws IOException {
		if (mensaje.equalsIgnoreCase("/cerrarServer")) {
			for (Servidor_Escuchador cliente : padre.getEscuchadores()) {
				PrintWriter outToClient = new PrintWriter(cliente.getSocket()
						.getOutputStream(), true);
				outToClient.println("Server say > " + mensaje
						+ padre.getHoraActual());
				cliente.getSocket().close();
				padre.getEscuchadores().remove(cliente);
			}
		} else {
			for (Servidor_Escuchador cliente : padre.getEscuchadores()) {
				PrintWriter outToClient = new PrintWriter(cliente.getSocket()
						.getOutputStream(), true);
				outToClient.println("Server say > " + mensaje
						+ padre.getHoraActual());

			}
		}
	}

	// escribe a una unica persona el servidor
	public void escribe(Servidor_Escuchador cliente, String mensaje)
			throws IOException {
		if (mensaje.equalsIgnoreCase("/salir")) {
			PrintWriter outToClient = new PrintWriter(cliente.getSocket()
					.getOutputStream(), true);
			outToClient.println("Server say > " + mensaje
					+ padre.getHoraActual());
			cliente.getSocket().close();
			padre.getEscuchadores().remove(cliente);
		} else {
			PrintWriter outToClient = new PrintWriter(cliente.getSocket()
					.getOutputStream(), true);
			outToClient.println("Server say > " + mensaje
					+ padre.getHoraActual());
		}
	}

	// un usuario hablando a todos.
	public void escribeGeneral(Servidor_Escuchador cliente, String mensaje)
			throws IOException {
		for (Servidor_Escuchador oyente : padre.getEscuchadores()) {
			PrintWriter outToClient = new PrintWriter(oyente.getSocket()
					.getOutputStream(), true);
			outToClient.println(cliente.getNombre() + " say > " + mensaje
					+ padre.getHoraActual());
		}
	}

	// para mensajes de configuración
	public void escribeConfiguracion(Servidor_Escuchador cliente, String mensaje)
			throws IOException {
		PrintWriter outToClient = new PrintWriter(cliente.getSocket()
				.getOutputStream(), true);
		outToClient.println(mensaje);
	}

	// el cliente escribe a una persona, otro cliente.
	public void escribe(Servidor_Escuchador enviador,
			Servidor_Escuchador cliente, String mensaje) throws IOException {
		PrintWriter outToClient = new PrintWriter(cliente.getSocket()
				.getOutputStream(), true);
		outToClient.println(enviador.getNombre() + " > " + mensaje
				+ padre.getHoraActual());
	}

	@Override
	public void run() {
		try {
			String toclient;
			BufferedReader inFromUser = new BufferedReader(
					new InputStreamReader(System.in));
			boolean serverAbierto = true;
			while (serverAbierto) {
				toclient = inFromUser.readLine();
				for (Servidor_Escuchador cliente : padre.getEscuchadores()) {
					PrintWriter outToClient = new PrintWriter(cliente
							.getSocket().getOutputStream(), true);
					if (toclient.equals("/cerrarServer")
							|| toclient.equals("Q")) {
						outToClient.println("/salir");
						cliente.getSocket().close();
						serverAbierto = false;
					} else {
						outToClient.println("Server say > " + toclient
								+ padre.getHoraActual());
					}
				}
			}
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
