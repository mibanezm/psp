package UT4_Servicios.E08_Chat.tcp.server;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.CopyOnWriteArrayList;

class TCP3_Servidor {
	private ArrayList<Servidor_Escuchador> escuchadores;
	private Servidor_Escritor escritor;
	private ArrayList<Servidor_Escuchador> administradores;
	private Calendar horaServer = Calendar.getInstance();
	private String pwd = "root";

	public TCP3_Servidor(ServerSocket server) {
		escuchadores = new ArrayList<Servidor_Escuchador>();
		administradores = new ArrayList<Servidor_Escuchador>();
		escritor = new Servidor_Escritor(this);
		(new Thread(escritor)).start();
	}

	public void nuevoCliente(Socket cliente) throws IOException {
		limpiaClientes();
		escuchadores.add(new Servidor_Escuchador(this, cliente));
		new Thread(escuchadores.get(escuchadores.size() - 1)).start();
	}

	public void limpiaClientes() {
		CopyOnWriteArrayList<Object> toRemove = new CopyOnWriteArrayList<Object>();
		for (Servidor_Escuchador escuchador : escuchadores) {
			if (escuchador.getSocket().isClosed())
				toRemove.add(escuchador);
		}
		escuchadores.removeAll(toRemove);
	}

	public static void main(String argv[]) throws Exception {
		ServerSocket Server = null;
		try {
			Server = new ServerSocket(Integer.parseInt(argv[0]));
		} catch (NumberFormatException error) {
			System.out.println("El puerto esta mal introducido");
			System.exit(0);
		}
		TCP3_Servidor Controlador = new TCP3_Servidor(Server);
		System.out.println("TCPServer Esperando cliente en puerto" + argv[0]);

		while (true) {
			Socket connected = Server.accept();
			System.out.println(" El cliente" + " " + connected.getInetAddress()
					+ ":" + connected.getPort() + " se ha conectado ");
			Controlador.nuevoCliente(connected);
		}
	}

	public ArrayList<Servidor_Escuchador> getEscuchadores() {
		return escuchadores;
	}

	public Servidor_Escritor getEscritor() {
		return escritor;
	}

	public ArrayList<Servidor_Escuchador> getAdministradores() {
		return administradores;
	}

	public Calendar getHoraServer() {
		return horaServer;
	}

	public String getPwd() {
		return pwd;
	}

	public String getHoraActual() {
		return "       Escrito a las:  " + horaServer.get(Calendar.HOUR_OF_DAY)
				+ ":" + horaServer.get(Calendar.MINUTE) + ":"
				+ horaServer.get(Calendar.SECOND);
	}
}