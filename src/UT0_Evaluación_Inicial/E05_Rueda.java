package UT0_Evaluación_Inicial;

//incluye ejemplo de Cloneable

public class E05_Rueda implements Cloneable {

	private String marca;
	private int diametroPulgadas;
	private int anchuraNominalMm;
	private int ratioAspectoPc;
	private static final int limiteKm = 60000;
	private int rodaduraKm = 0;
	private boolean pinchada = false;
	private boolean cambiar = false;

	E05_Rueda(String marca, int diametroPulgadas, int anchuraNominalMm,
			int ratioAspectoPc) {
		this.marca=marca;
		this.diametroPulgadas = diametroPulgadas;
		this.anchuraNominalMm = anchuraNominalMm;
		this.ratioAspectoPc = ratioAspectoPc;
	}

	// Constructor por defecto
	E05_Rueda() {
		this("",16,205,55);
	}
	
	// Constructor por copia
	E05_Rueda(E05_Rueda r) {
		this.marca=r.marca;
		this.diametroPulgadas=r.diametroPulgadas;
		this.anchuraNominalMm=r.anchuraNominalMm;
		this.ratioAspectoPc=r.ratioAspectoPc;
		this.rodaduraKm=r.rodaduraKm;
		this.pinchada=r.pinchada;
		this.cambiar=r.cambiar;
	}
	
    protected Object clone() throws CloneNotSupportedException {
    	//Clonar la superclase
    	E05_Rueda clon=(E05_Rueda)super.clone();
    	//clonar el resto (shallow copy)
    	//clon.objeto=(objeto)propiedad.clone();  //para campos "mutable"
		clon.marca=marca;  //no necesita copia defensiva, String es inmutable
		clon.diametroPulgadas=diametroPulgadas;
		clon.anchuraNominalMm=anchuraNominalMm;
		clon.ratioAspectoPc=ratioAspectoPc;
		clon.rodaduraKm=rodaduraKm;
		clon.pinchada=pinchada;
		clon.cambiar=cambiar;
		//salir
		return clon;
    }

	public void rodar(int km) throws E05_RuedaPinchadaException {
		if (! pinchada) {
			this.rodaduraKm += km;
			if (rodaduraKm >= limiteKm) {
				cambiar = true;
			}
		} else
			throw new E05_RuedaPinchadaException("Esta pinchada, no puedes rodar!");
	}

	public void pinchar() {
		pinchada = true;
	}
	
	public boolean esta_pinchada() {
		return pinchada;
	}

	public void reparar() {
		pinchada = false;
	}
	
	public void print() {
		int difKm=limiteKm-rodaduraKm;
		System.out.println("Marca: \""+marca+'"');
		System.out.println("Diámetro: "+diametroPulgadas+'"');
		System.out.println("Anchura: "+anchuraNominalMm+" mm");
		System.out.println("Relación de Aspecto: "+ratioAspectoPc+'%');
		if ( (difKm) > 0)
			System.out.println("Km: "+rodaduraKm+" restan "+difKm);
		else
			System.out.println("Km: "+rodaduraKm+" excedidos "+ (-difKm));
		System.out.println("Pinchada: "+pinchada);
		System.out.println("Cambiar: "+cambiar);
	}
	
	public void println() {
		this.print();
		System.out.print('\n');
	}
	
	public static void main(String[] args) {
		E05_Rueda r1=new E05_Rueda(), r2=new E05_Rueda("Michelin",14,165,65);
		try {
			r1.rodar(75000);
			r1.pinchar();
			r2.rodar(34500);
			r1.rodar(5);
			r2.rodar(100);
			r2.pinchar();
			r1.reparar();
		}
		catch (E05_RuedaPinchadaException e)
		{
			System.out.println(e.getMessage());
		}
		r1.println();
		r2.println();
		E05_Rueda r3=new E05_Rueda(r2);	 //copia profunda
		r3.println();
	}
}