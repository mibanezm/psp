package UT2_Multihilo;

public class P10_VasoRellenador implements Runnable {

	private float cantidad;	//para memorizar cuanto se ha de rellenar
	private P10_Vaso p10_Vaso;			//para memorizar qué se ha de rellenar
	private static boolean mensajes;	//permite ver/ocultar mensajes de como está el p10_Vaso

	public P10_VasoRellenador(float cantidad, P10_Vaso p10_Vaso) {
		this.cantidad=cantidad;
		this.p10_Vaso=p10_Vaso;
	}

	public void run() {
		synchronized (p10_Vaso) {
			//el acceso al p10_Vaso es seguro porque sus métodos son sincronizados.
			//sin embargo, esto es necesario para que no se entremezclen los mensajes
			//probar que pasa si se commenta este synchronized(p10_Vaso)
			if (mensajes)
				System.out.print(p10_Vaso.getContenido()+"+"+cantidad+"=");
			try {
				p10_Vaso.rellenar(cantidad);
				if (mensajes)
					System.out.println(p10_Vaso.getContenido());
			} catch (Exception e) {};
		}
	}

	public static void main(String[] args) throws Exception {
		int hilos=1000;
		
		//necesitamos conservar punteros a los hilos para luego poder haer el join() correctamente
		Thread rellenador[]= new Thread[hilos];
		Thread vaciador[]= new Thread[hilos];

		final float capacidad= 100, contenido = 50;
		final float inc=(float)0.5;

		//Crear un único P10_Vaso, no agitado, de 100 litros máx! (por poner algo)
		P10_Vaso v = new P10_Vaso(false,capacidad,contenido);       

		mensajes=false;		//trabajar silenciosamente, probar que pasa al cambiarlo
		
		for (int i = 0; i < hilos; i++) {
			//hilos rellenadores, alta prioridad
			(rellenador[i]=new Thread(new P10_VasoRellenador(inc,v))).start();
			rellenador[i].setPriority(Thread.MAX_PRIORITY);
			//hilos vaciadores, baja prioridad
			//con la intención de evitar que el p10_Vaso llegue a vaciarse
			(vaciador[i]=new Thread(new P10_VasoRellenador(-inc,v))).start();
			rellenador[i].setPriority(Thread.MIN_PRIORITY);
		}

		//reunir con sus hilos
		for (int i = 0; i < hilos; i++) {
			rellenador[i].join();
			vaciador[i].join();
		}

		//Comprobacion final del contenido
		v.verContenido();
	}
}
