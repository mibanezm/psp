package UT2_Multihilo;

import UT0_Repaso.CCC;
import UT0_Repaso.Fecha;

/* Acceso NO concurrente a cuenta bancaria
 */

public class P00_CuentaBancariaNoSync {
	private CCC cuenta;
	private Fecha fechaApertura;
	private Double saldo;

	P00_CuentaBancariaNoSync(CCC cuenta, Fecha fechaApertura, double saldo) {
		this.cuenta=cuenta;
		this.fechaApertura=fechaApertura;
		this.saldo=saldo;
	}

	public void print() {
		System.out.println("Cuenta="+cuenta+" Fecha Apertura="+fechaApertura+" Saldo="+saldo);
	}

	public void ingresar(double dinero) {
			this.saldo+=dinero;
	}

	public void reintegrar(double dinero) throws Exception {
			if (dinero<this.saldo) {
				this.saldo-=dinero;
			} else
				throw new Exception("Saldo insuficiente: sacando "+dinero+" y solo hay "+this.saldo);
	}

	public double getSaldo() {
		return saldo;
	}

}
