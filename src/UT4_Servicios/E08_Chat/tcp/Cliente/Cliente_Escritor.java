package UT4_Servicios.E08_Chat.tcp.Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Cliente_Escritor implements Runnable {
	private Socket server;
//	private TCP2_Cliente padre;
	private String mensajeAServer;
	PrintWriter escritorAlServer;

	public Cliente_Escritor(TCP2_Cliente padre, Socket servidor) {
		server = servidor;
//		this.padre = padre;
	}

	public void escribe(String mensaje) throws IOException {
		if (mensajeAServer.equalsIgnoreCase("quit")) {
			escritorAlServer.println(mensajeAServer);
			server.close();
		} else {
			escritorAlServer.println(mensajeAServer);
		}
	}

	@Override
	public void run() {
		try {
			// escribo al server
			BufferedReader entradaDeTeclado = new BufferedReader(
					new InputStreamReader(System.in));
			escritorAlServer = new PrintWriter(server.getOutputStream(), true);

			while (true) {
				mensajeAServer = entradaDeTeclado.readLine();

				if (mensajeAServer.equalsIgnoreCase("/salir")) {
					escritorAlServer.println(mensajeAServer);
					server.close();
					break;
				} else {
					escritorAlServer.println(mensajeAServer);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
