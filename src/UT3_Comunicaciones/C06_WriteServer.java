package UT3_Comunicaciones;

import java.net.*; 
 
class C06_WriteServer { 

  /* Los puertos 0..1023 son privilegiados, solo root puede hacer binding */

  public static int serverPort = 2998; 
  public static int clientPort = 2999; 
  public static int buffer_size = 1024; 
  public static DatagramSocket ds; 
  public static byte buffer[] = new byte[buffer_size]; 
 
  public static void TheServer() throws Exception { 
    int pos=0; 
    while (true) { 
      int c = System.in.read(); 
      switch (c) { 
        case -1:  
          System.out.println("Server Quits."); 
          ds.close();
          return; 
        case '\r':  
          break; 
        case '\n': 
          ds.send(new DatagramPacket(buffer,pos, 
             InetAddress.getLocalHost(),clientPort)); 
          pos=0; 
          break; 
        default: 
          buffer[pos++] = (byte) c; 
      } 
    } 
  } 
 
  public static void TheClient() throws Exception { 
    while(true) { 
      DatagramPacket p = new DatagramPacket(buffer, buffer.length); 
      ds.receive(p); 
      System.out.println(new String(p.getData(), 0, p.getLength())); 
    } 
  } 
 
  public static void main(String args[]) throws Exception { 
    if(args.length == 1) {
      System.out.println("Servidor en puerto "+serverPort);
      ds = new DatagramSocket(serverPort); 
      TheServer(); 
    } else {
      System.out.println("Cliente en puerto "+clientPort);
      ds = new DatagramSocket(clientPort); 
      TheClient(); 
    } 
  } 
}
