package UT4_Servicios.udp.server;

import java.net.*;
import java.util.Calendar;

class UDP_Servidor {
	private Servidor_Escritor escritor;
	private Servidor_Escuchador escuchador;
	private Calendar horaServer = Calendar.getInstance();
	private String pwd = "root";
	private DatagramSocket socket;
	private byte[] buffer;
	private DatagramPacket clientes;
	
	public UDP_Servidor(String puerto) {
		try {
			socket = new DatagramSocket(Integer.parseInt(puerto));
			buffer = new byte[65536];
			clientes = new DatagramPacket(buffer, buffer.length);
		} catch (NumberFormatException error) {
			System.out.println("El puerto esta mal introducido");
			System.exit(0);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		escritor = new Servidor_Escritor(this);
		escuchador = new Servidor_Escuchador(this);
		(new Thread(escritor)).start();
		(new Thread(escuchador)).start();
	}


	public static void main(String argv[]) throws Exception {

		//UDP_Servidor Controlador = new UDP_Servidor(argv[0]);
		System.out.println("TCPServer Esperando cliente en puerto" + argv[0]);
	}

	public Servidor_Escritor getEscritor() {
		return escritor;
	}

	public Calendar getHoraServer() {
		return horaServer;
	}

	public String getPwd() {
		return pwd;
	}

	public String getHoraActual() {
		return "       Escrito a las:  " + horaServer.get(Calendar.HOUR_OF_DAY)
				+ ":" + horaServer.get(Calendar.MINUTE) + ":"
				+ horaServer.get(Calendar.SECOND);
	}


	/**
	 * @return the escuchador
	 */
	public Servidor_Escuchador getEscuchador() {
		return escuchador;
	}


	/**
	 * @return the socket
	 */
	public DatagramSocket getSocket() {
		return socket;
	}


	/**
	 * @return the buffer
	 */
	public byte[] getBuffer() {
		return buffer;
	}


	/**
	 * @return the incoming
	 */
	public DatagramPacket getCliente() {
		return clientes;
	}
}