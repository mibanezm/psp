package UT0_Evaluación_Inicial;


public class E05_Motor {
	private int cubicajeCC;
	private int potenciaCV;
	private static final int limiteKm = 300000;
	private int rodaduraKm = 0;

	E05_Motor(int cubicajeCC, int potenciaCV) {
		this.cubicajeCC=cubicajeCC;
		this.potenciaCV=potenciaCV;
	}

	// E05_Motor Estándar
	E05_Motor() {
		this(1900,105);
	}

	public void rodar(int km) throws Exception {
		rodaduraKm+=km;
		if (rodaduraKm>limiteKm)
			throw new Exception("E05_Motor agotado!");
	}

	public void print() {
		int difKm=limiteKm-rodaduraKm;
		System.out.println("Cubicaje: "+cubicajeCC+" cc");
		System.out.println("Potencia: "+potenciaCV+" CV");
		if ( difKm > 0)
			System.out.println("Km: "+rodaduraKm+" restan "+difKm);
		else
			System.out.println("Km: "+rodaduraKm+" excedidos "+ (-difKm));
	}
	
	public void println() {
		this.print();
		System.out.print('\n');
	}
	
	public static void main(String[] args) {
		E05_Motor m=new E05_Motor();
		try {
			m.rodar(1000);
			m.rodar(250000);
			m.rodar(50000);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		m.println();
	}
}
