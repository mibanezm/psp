package UT4_Servicios.udp.cliente;

import java.net.*;
import java.util.Scanner;

class UDP_Cliente {
	private Cliente_Escritor escritor;
	private Cliente_Escuchador escuchador;
	private InetAddress direccion;
	private String puerto;
	private DatagramSocket sock;

	public UDP_Cliente(InetAddress direccion, String puerto) {
		this.direccion = direccion;
		this.puerto = puerto;
		try {
			sock = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		escuchador = new Cliente_Escuchador(this);
		(new Thread(escuchador)).start();

		escritor = new Cliente_Escritor(this);
		(new Thread(escritor)).start();
	}

	public static void main(String args[]) throws Exception {
		Scanner entrada = new Scanner(System.in);
		InetAddress direccion = null;
		String puerto = null;
		try {
			if (args.length > 1) {
				puerto = args[1];
				direccion = InetAddress.getByName(args[0]);
			} else {
				System.out.println("Introduzca la ip del servidor: ");
				String host = entrada.next();
				System.out.println("Introduzca el puerto del servidor: ");
				puerto = entrada.next();
				direccion = InetAddress.getByName(host);
			}
		} catch (NumberFormatException error) {
			System.err
					.println("El puerto introducido no es válido, introduce únicamente números, error en la conexión.");
			System.exit(1);
		}
		new UDP_Cliente(direccion, puerto);

	}


	/**
	 * @return the direccion
	 */
	public InetAddress getDireccion() {
		return direccion;
	}

	/**
	 * @return the puerto
	 */
	public String getPuerto() {
		return puerto;
	}

	/**
	 * @return the sock
	 */
	public DatagramSocket getSock() {
		return sock;
	}
}