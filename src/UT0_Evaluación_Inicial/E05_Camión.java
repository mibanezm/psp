package UT0_Evaluación_Inicial;


public class E05_Camión extends E05_Coche {
	private double tara;
	private double pma;
	private E05_Rueda [] rr;

	E05_Camión(Double tara, Double pma, int numRuedasRemolque) {
		super();
		this.tara=tara;
		this.pma=pma;
		this.rr=new E05_Rueda[numRuedasRemolque];
		for (int i = 0; i < numRuedasRemolque; i++)
			rr[i]=new E05_Rueda();
	}

	E05_Camión() {
		this(8500.,24000.,6);
	}

	public void rodar(int km) throws Exception {
		super.rodar(km);
		for (int i = 0; i < rr.length; i++)
				rr[i].rodar(km);
	}
	
	public void print() {
		super.print();
		System.out.println("");
		System.out.println("Tara: "+tara+" kg");
		System.out.println("PMA: "+pma+" kg");
		for (int i=0; i<rr.length; i++)
			rr[i].println();
	}
	
	public void println() {
		this.print();
		System.out.print('\n');
	}
	
	public static void main(String[] args) {
		E05_Camión c=new E05_Camión();
		try {
			c.rodar(350000);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		c.println();
	}
}
