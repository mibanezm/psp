package UT2_Multihilo;

public class P10_Vaso extends P10_Botella {

	/*NUEVAS PROPIEDADES (acceso privado):
     boolean agitado (true / false). Representa si hemos agitado nuestra bebida o no.
	 */    
	boolean agitado;

	//Constructores

	public P10_Vaso(float capacidad, float contenido, Material material, boolean agitado) throws Exception {
		super(capacidad, contenido, material);
		this.agitado = agitado;
	}

	public P10_Vaso(boolean agitado, float capacidad) throws Exception {
		super(capacidad,0,Material.PLASTICO);
		this.agitado = false;
	}

	public P10_Vaso (boolean agitado, float capacidad, float contenido) throws Exception{
		super(capacidad,contenido,Material.PLASTICO);
		this.agitado=false;
	}

	public P10_Vaso(P10_Vaso copia) throws Exception{
		this(copia.getCapacidad(),copia.getContenido(),copia.getMaterial(), copia.getAgitado());
	}

	//setter's, getter's, is'
	public boolean isAgitado() {
		return agitado;
	}

	public void setAgitado(boolean agitado) {
		this.agitado = agitado;
	}

	public boolean getAgitado() {
		return agitado;
	}    

	//agitar(): si el vaso tiene algo, lo agita. Tarda 3 segundos en estar agitado
	public synchronized void agitar() throws Exception {
		if(getContenido()>0){
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) { };
			setAgitado(true);
		}
		else
			throw new Exception();
	}

	//reposar(): a los 10 segundos marca el vaso como no agitado
	public synchronized void reposar(){
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) { }
		finally {
			setAgitado(false);
		}
	}

	//beber(): vacia el vaso, si no está agitado. Si lo está, espera un segundo 
	public synchronized void beber(float litros) throws Exception {
		while (getAgitado())
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) { }
		super.vaciar(litros);
	}

	//sobrecargo este método para que el mensaje diga "vaso" en lugar de "botella"
	//(si eso me diese igual podría quitar este método)
	public synchronized void verContenido(){
		System.out.println("El contenido actual del vaso es " + getContenido() + " litros");
	}
}
