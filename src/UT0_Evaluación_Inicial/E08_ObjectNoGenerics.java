package UT0_Evaluación_Inicial;

public class E08_ObjectNoGenerics {
	Object ob;
	E08_ObjectNoGenerics(Object o) {
		ob=o;
	}
	Object get() {
		return ob;
	}
	void showType() {
		System.out.println("Tipo de T: "+ob.getClass().getName());
	}

	public static void main(String args[]) {
		//con Integer:
		E08_ObjectNoGenerics iOb=new E08_ObjectNoGenerics(123);
		iOb.showType();
		int v=(Integer)iOb.get();
		System.out.println("Valor: "+v);

		//con String:
		E08_ObjectNoGenerics sOb=new E08_ObjectNoGenerics("Hola, que tal");
		sOb.showType();
		String s=(String)sOb.get();
		System.out.println("Valor: "+s);

		//Curiosidades
		iOb=sOb;	//Esto compila, aunque no tiene sentido
		v=(Integer)iOb.get();

		/* Produce error runtime:
		java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
		 */
	}
}
