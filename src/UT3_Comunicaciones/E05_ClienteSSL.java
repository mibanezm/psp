package UT3_Comunicaciones;

import javax.net.ssl.*;
import javax.net.*; 
import java.net.*;
import java.io.*;

public class E05_ClienteSSL {
	public static void main (String args[]) throws IOException {

		int serverPort = 3000;

		if (args.length < 1) {
			System.out.println("java factorySSLSocketClient serverHost serverPort");
			System.out.println("serverPort defaults to 3000 if not specified.");
			return;
		}
		if (args.length == 2)
			serverPort = new Integer(args[1]).intValue();

		System.out.println("Connecting to host " + args[0] + " at port " +
				serverPort);

		// Change this to create an SSLSocketFactory instead of a SocketFactory.
		SocketFactory socketFactory = SSLSocketFactory.getDefault();

		// We do not need to change anything else.
		// That's the beauty of using factories!
		Socket  s = socketFactory.createSocket(args[0], serverPort);
		// El resto del programa aqui.
		s.close();
	}
}