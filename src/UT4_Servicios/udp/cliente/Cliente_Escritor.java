package UT4_Servicios.udp.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;


public class Cliente_Escritor implements Runnable {
//	private Socket server;
	private UDP_Cliente padre;
//	private String ToServer;
	PrintWriter outToServer;

	public Cliente_Escritor(UDP_Cliente padre) {
		this.padre = padre;

	}

	@Override
	public void run() {
		try {
			BufferedReader entradaDeTeclado = new BufferedReader(
					new InputStreamReader(System.in));

			while (true) {

				System.out.println("Introduce el mensaje que quieres enviar :");
				String mensaje = (String) entradaDeTeclado.readLine();
				byte[] bmensaje = mensaje.getBytes();

				DatagramPacket paqueteConMensaje = new DatagramPacket(bmensaje, bmensaje.length,
						padre.getDireccion(), Integer.parseInt(padre
								.getPuerto()));
				padre.getSock().send(paqueteConMensaje);

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
