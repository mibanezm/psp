package UT0_Evaluación_Inicial;

//Excepcion para ruedas

public class E05_RuedaPinchadaException extends Exception {
	
	static final long serialVersionUID=1L;

	E05_RuedaPinchadaException(String msg) {
		super(msg);
	}
}