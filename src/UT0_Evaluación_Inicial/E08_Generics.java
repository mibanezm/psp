package UT0_Evaluación_Inicial;

public class E08_Generics<T> {
	T ob;
	E08_Generics(T o) {
		ob=o;
	}
	T get() {
		return ob;
	}
	void showType() {
		System.out.println("Tipo de T: "+ob.getClass().getName());
	}

	public static void main(String args[]) {
		//con Integer:
		E08_Generics<Integer> iOb=new E08_Generics<Integer>(123);
		iOb.showType();
		int v=iOb.get();
		System.out.println("Valor: "+v);

		//con String:
		E08_Generics<String> sOb=new E08_Generics<String>("Hola, que tal");
		sOb.showType();
		String s=sOb.get();
		System.out.println("Valor: "+s);
	}
}
