package UT4_Servicios;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class E01_ClienteTCP {
	private Socket socketServicio;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private String mensaje;
	private InetSocketAddress isa;

	public E01_ClienteTCP(String host, int puerto) {
		isa = new InetSocketAddress(host, puerto);
	}

	void run() {
		try {
			socketServicio = new Socket(isa.getHostName(), isa.getPort());/* Handshake */
			System.out.printf("Conectado a %s, puerto %d\n", isa.getHostName(),
					isa.getPort());
			try {
				out = new ObjectOutputStream(socketServicio.getOutputStream());
				out.flush();
			} catch (IOException ioe) {
				System.err.println("Erron en la salida hacia el servidor");
				throw new IOException();
			}
			try {
				in = new ObjectInputStream(socketServicio.getInputStream());
			} catch (IOException ioe) {
				System.err.println("Erron en la entrada desde el servidor");
				throw new IOException();
			}
			do {
				try {
					mensaje = (String) in.readObject();
					System.out.println("servidor>" + mensaje);
					enviarMensaje("Hola, servidor");
					mensaje = "adiós";
					enviarMensaje(mensaje);
				} catch (ClassNotFoundException classNot) {
					System.err
							.println("Datos recibidos en formato desconocido");
				}
			} while (!mensaje.equals("adiós"));
		} catch (ConnectException ce) {
			System.err.println("Servidor caído");
			System.exit(1);
		} catch (UnknownHostException unknownHost) {
			System.err.printf("Host desconodico: %s\n", isa.getHostName());
			System.exit(1);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			try {
				if (socketServicio != null) {
					in.close();
					out.close();
					socketServicio.close();
				}
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	void enviarMensaje(String msg) {
		try {
			out.writeObject(msg);
			out.flush();
			System.out.println("cliente>" + msg);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	public static void main(String args[]) {
		try {
			E01_ClienteTCP cliente = new E01_ClienteTCP(args[0],
					Integer.parseInt(args[1]));
			cliente.run();
		} catch (NumberFormatException nfe) {
			System.err
					.println("El segundo argumento tiene que ser un número entero");
			System.exit(1);
		} catch (IndexOutOfBoundsException ioobe) {
			System.err
					.println("Hay que introducir dos argumentos: host y puerto");
			System.exit(1);
		}
	}
}