package UT3_Comunicaciones;

import java.io.*;
import java.net.*;

public class E08_Server{
	ServerSocket socketServidor;
	Socket conexión = null;
	ObjectOutputStream out;
	ObjectInputStream in;
	String mensaje;
	E08_Server(){}
	void run()
	{
		try{
			socketServidor = new ServerSocket(1210, 10);
			System.out.println("Esperando conexión entrante");
			conexión = socketServidor.accept();
			System.out.println("Conexión recibida desde " + conexión.getInetAddress().getHostName());
			out = new ObjectOutputStream(conexión.getOutputStream());
			out.flush();
			in = new ObjectInputStream(conexión.getInputStream());
			enviarMensaje("Conexión Ok");
			do{
				try{
					mensaje = (String)in.readObject();
					System.out.println("cliente>" + mensaje);
					if (mensaje.equals("adiós"))
						enviarMensaje("adiós");
				}
				catch(ClassNotFoundException classNot){
					System.err.println("Datos recibidos en formato desconocido");
				}
			}while(!mensaje.equals("adiós"));
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
		finally {
			try {
				in.close();
				out.close();
				socketServidor.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
	}
	void enviarMensaje(String msg)
	{
		try{
			out.writeObject(msg);
			out.flush();
			System.out.println("servidor>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	public static void main(String args[])
	{
		E08_Server servidor = new E08_Server();
		while(true){
			servidor.run();
		}
	}
}
