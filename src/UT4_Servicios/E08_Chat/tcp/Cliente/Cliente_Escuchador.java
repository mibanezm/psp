package UT4_Servicios.E08_Chat.tcp.Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

public class Cliente_Escuchador implements Runnable {
	private Socket server;
	private TCP2_Cliente padre;

	public Cliente_Escuchador(TCP2_Cliente padre, Socket servidor) {
		server = servidor;
		this.padre = padre;
	}

	@Override
	public void run() {
		try {
			BufferedReader inFromServer;

			inFromServer = new BufferedReader(new InputStreamReader(
					server.getInputStream()));

			String FromServer;
			while (true) {
				FromServer = inFromServer.readLine();
				try {
					padre.getHoraServer().setTimeInMillis(
							Long.parseLong(FromServer));
					break;
				} catch (NumberFormatException error) {

				}
			}
			while (true) {
				// Leo del server
				try {
					FromServer = inFromServer.readLine();

					if (FromServer.equalsIgnoreCase("/salir")) {
						server.close();
						break;
					} else {
						System.out.println(FromServer);
					}
				} catch (SocketException error) {
					System.err
							.println("Servidor ha cerrado la conexion inesperadamente");
					server.close();
					break;
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
