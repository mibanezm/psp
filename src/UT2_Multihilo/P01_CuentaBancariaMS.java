package UT2_Multihilo;

import UT0_Repaso.CCC;
import UT0_Repaso.Fecha;

/* Acceso concurrente a cuenta bancaria
 * mediante Métodos Sincronizados (MS)
 */

public class P01_CuentaBancariaMS {
	private CCC cuenta;
	private Fecha fechaApertura;
	private Double saldo;

	P01_CuentaBancariaMS(CCC cuenta, Fecha fechaApertura, double saldo) {
		this.cuenta=cuenta;
		this.fechaApertura=fechaApertura;
		this.saldo=saldo;
	}

	public void print() {
		System.out.println("Cuenta="+cuenta+" Fecha Apertura="+fechaApertura+" Saldo="+saldo);
	}

	public synchronized void ingresar(double dinero) {
			this.saldo+=dinero;
	}

	public synchronized void reintegrar(double dinero) throws Exception {
			if (dinero<this.saldo) {
				this.saldo-=dinero;
			} else
				throw new Exception("Saldo insuficiente: sacando "+dinero+" y solo hay "+this.saldo);
	}

	public synchronized double getSaldo() {
		return saldo;
	}

}
