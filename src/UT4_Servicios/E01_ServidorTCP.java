package UT4_Servicios;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class E01_ServidorTCP {
	private ServerSocket socketServidor;
	private int puerto;

	public E01_ServidorTCP(int puerto) {
		this.puerto = puerto;
	}

	public static class Cliente extends Thread {
		private Socket s;
		private ObjectOutputStream out;
		private ObjectInputStream in;
		private String mensaje;

		public Cliente(Socket s) {
			this.s = s;
		}

		@Override
		public void run() {
			try {
				System.out.printf("Conexión recibida desde %s:%d\n", s
						.getInetAddress().getHostName(), s.getPort());
				try {
					out = new ObjectOutputStream(s.getOutputStream());
					out.flush();
				} catch (IOException ioe) {
					System.err.println("Erron en la salida hacia el cliente");
					throw new IOException();
				}
				try {
					in = new ObjectInputStream(s.getInputStream());
				} catch (IOException ioe) {
					System.err.println("Erron en la entrada desde el cliente");
					throw new IOException();
				}
				enviarMensaje("Conexión Ok");
				do {
					try {
						mensaje = (String) in.readObject();
						System.out.printf("cliente desde %s:%d>%s\n", s
								.getInetAddress().getHostName(), s.getPort(),
								mensaje);
						if (mensaje.equals("adiós"))
							enviarMensaje("adiós");
					} catch (ClassNotFoundException classNot) {
						System.err
								.println("Datos recibidos en formato desconocido");
					}
				} while (!mensaje.equals("adiós"));
			} catch (IOException ioException) {
				ioException.printStackTrace();
			} finally {
				try {
					if (s != null) {
						in.close();
						out.close();
						s.close();
					}
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
			}
		}

		void enviarMensaje(String msg) {
			try {
				out.writeObject(msg);
				out.flush();
				System.out.printf("servidor hacia %s:%d>%s\n", s
						.getInetAddress().getHostName(), s.getPort(), msg);
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	public void run() {
		try {
			socketServidor = new ServerSocket(puerto, 10);
			while (true) {
				try {
					System.out.println("Esperando conexión entrante");
					new Cliente(socketServidor.accept()).start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			System.err.printf(
					"No se puede crear el servidor en el puerto %d\n", puerto);
			System.exit(1);
		}

	}

	public static void main(String args[]) {
		try {
			E01_ServidorTCP servidor = new E01_ServidorTCP(Integer.parseInt(args[0]));
			servidor.run();
		} catch (NumberFormatException nfe) {
			System.err
					.println("El primer argumento tiene que ser un número entero");
			System.exit(1);
		} catch (IndexOutOfBoundsException ioobe) {
			System.err.println("Hay que introducir el puerto como argumento");
			System.exit(1);
		}
	}
}
