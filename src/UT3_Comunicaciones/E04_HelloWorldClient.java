package UT3_Comunicaciones;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class E04_HelloWorldClient {

    //private static final String CLASSPATH = "./bin";
    //private static final String WORKING_DIRECTORY = ".";
 
    public void startJavaProcess() throws Exception {
    	
    	
    	/*
    	 * Descomentar este bloque si se quiere que el cliente cree el proceso del servidor
    	
        final E04_JavaProcessBuilder e04_JavaProcessBuilder = new E04_JavaProcessBuilder();
        if (System.getProperty("os.name").toLowerCase().equals("mac os x")) {
            e04_JavaProcessBuilder.setJavaRuntime("/System/Library/Frameworks/JavaVM.framework/Versions/1.6/Home/bin/java");
        }
        e04_JavaProcessBuilder.setWorkingDirectory(WORKING_DIRECTORY);
        e04_JavaProcessBuilder.addClasspathEntry(CLASSPATH);
        e04_JavaProcessBuilder.setMainClass("UT3_Comunicaciones.E04_HelloWorldServer");
        e04_JavaProcessBuilder.addArgument(String.valueOf(E04_JavaProcessBuilder.PUERTO));
        final Process process = e04_JavaProcessBuilder.startProcess();
    	 */
       
        //BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

        //Este hilo para que el cliente mande comandos al servidor
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    // Thread.sleep(1000);   //Temporizador, inccecesario si el servidor ya está lanzado
                	
                	//conectar al servidor
                    Socket socket = new Socket("localhost", E04_JavaProcessBuilder.PUERTO);
                    
                    //Flujos de comunicación con el servidor
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    
                    //Mandar comando "hello"
                    System.out.println("CLIENT: Sending 'hello' command.");
                    out.println("hello");
                    
                    //imprimir respuesta al comando
                    System.out.println("CLIENT: Received '" + in.readLine() + "' from the server.");
                    
                    Thread.sleep(2000);
                    
                    //mandar comando "stop"
                    System.out.println("CLIENT: Sending 'stop' command.");
                    out.println("stop");
                    
                    //imprimir respuesta
                    System.out.println("CLIENT: Received '" + in.readLine() + "' from the server.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

        /*
        //El hilo "main" para que mostrar los mensajes del servidor, leidos mediante br, que recoge 
        //la salida estándar del proceso Server.
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println("SERVER: " + line);
        }
        */
        t.join();
    }
    
    public static void main(String [] args)
    {
    	E04_HelloWorldClient t=new E04_HelloWorldClient();
    	try
    	{
    		t.startJavaProcess();
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
    	}
    }
}
