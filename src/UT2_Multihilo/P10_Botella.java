package UT2_Multihilo;

public class P10_Botella extends Thread{
    public enum Material {
        PLASTICO, VIDRIO, METAL;
    };
    
    private float capacidad, contenido;
    private Material material;
    
    /*CONSTRUCTORES*/
    public P10_Botella(float capacidad, float contenido,  Material material) throws Exception {
        this.setBotella(capacidad, contenido, material);
    }
    
    public P10_Botella(float capacidad) throws Exception{
        this.setBotella(capacidad, contenido, material);
    }
    
    public P10_Botella(float capacidad, float contenido) throws Exception{
        this.setBotella(capacidad,contenido,Material.PLASTICO);
    }
    
    public P10_Botella(P10_Botella b) throws Exception{
        this.setBotella(b.capacidad,b.contenido,b.material);
    }
    
    public P10_Botella()  throws Exception{
        this.setBotella(0);
    }    
    
    
    /*GETTER's & SETTER's*/  
    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public synchronized float getContenido() {
        return contenido;
    }

    public synchronized void setContenido(float contenido) throws Exception {
            if(contenido>capacidad || contenido<0){
                throw new Exception("Se supera la capacidad");
            }
            this.contenido = contenido;
    }   
    
     public final synchronized void setBotella(float capacidad, float contenido,  Material material) throws Exception {
        if(capacidad<0){
            throw new Exception("Error:P10_Botella(float, float, Material) Capacidad negativa (" + capacidad + ")");
        }
        
        this.capacidad = capacidad;
        
         if(contenido<0 || contenido>capacidad){
            throw new Exception("Error:P10_Botella(float, float, Material) Contenido negativa (" + contenido + ")");
        }
        this.contenido = contenido;
        
        this.material = material;
    }
    
     public final synchronized void setBotella(float capacidad) throws Exception{
             setBotella(capacidad,0,Material.PLASTICO);
    }
     
     public final synchronized void setBotella(float capacidad, float contenido) throws Exception{
        setBotella(capacidad,contenido,Material.PLASTICO);
    }
    
     public final synchronized void setBotella(P10_Botella b) throws Exception{
        setBotella(b.capacidad,b.contenido,b.material);
    }
    
    public final synchronized void setBotella() throws Exception{
        setBotella(0);
    }   
    
    
    public void  rellenar(){
        try{
            setContenido(capacidad);
        }catch(Exception e){
            System.out.println("Error: rellenar(): Contenido incorrecto (" + capacidad+ ")");
        }
    }
    
    public synchronized void rellenar( float litros) throws Exception{
        setContenido(getContenido() + litros);
    }
    

    public void vaciar() throws Exception{
        setContenido(0);
    }
    
    
    public void vaciar( float litros) throws Exception{
        //No es sincronizado xq llama al rellenar que si lo es
        rellenar(-litros); 
    }
    
    
    public void verContenido(){
        System.out.println("El contenido actual de la botella es de " + getContenido() + " litros");
    }
     
    
     public static void main(String[] args) throws Exception{
        
        int hilos=100;
        Thread []miBotellaLL= new Thread[hilos];
        Thread []miBotellaV= new Thread[hilos];
        
        float capacidad= 100, contenido = 50;
        
        int posicion=1;
        
        
        P10_Botella b = new P10_Botella(capacidad, contenido);       
        
        float Cr= (float)0.2;
        float Cv= (float)0.2;
         for (int i = 0; i < hilos; i++) {
             System.out.println(posicion + ")");
             miBotellaLL[i]=new Thread(b);
             miBotellaLL[i].start();
             b.rellenar(Cr);
             
             miBotellaV[i]=new Thread(b);
             miBotellaV[i].start();
             b.vaciar(Cv);
             
             posicion+=1;
         }
        
         for (int i = 0; i < hilos; i++) {
             miBotellaLL[i].join();
             miBotellaV[i].join();
         }
        
         //Comprobacion final
         b.verContenido();
    }
     
}
