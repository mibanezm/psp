package UT0_Evaluación_Inicial;


public class E05_Coche {
	private E05_Motor e05_Motor;
	private E05_Rueda [] r=new E05_Rueda[5];

	E05_Coche() {
		e05_Motor = new E05_Motor();
		for (int i = 0; i < r.length; i++) {
			r[i] = new E05_Rueda();
		}
	}

	public void rodar(int km) throws Exception {
		e05_Motor.rodar(km);
		for (int i = 0; i < r.length; i++) {
			r[i].rodar(km);
		}
	}
	
	public void print() {
		e05_Motor.println();
		for (int i=0; i<r.length; i++)
			r[i].println();
	}
	
	public void println() {
		this.print();
		System.out.print('\n');
	}
	
	public static void main(String[] args) {
		E05_Coche c=new E05_Coche();
		try {
			c.rodar(1000);
			c.rodar(2000);
			c.rodar(512);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		c.println();
	}
}
