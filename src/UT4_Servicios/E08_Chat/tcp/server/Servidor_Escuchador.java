package UT4_Servicios.E08_Chat.tcp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

public class Servidor_Escuchador implements Runnable {
	private Socket socketCliente;
	private TCP3_Servidor padre;
	private String nombre = "";

	public Servidor_Escuchador(TCP3_Servidor padre, Socket servidor) {
		socketCliente = servidor;
		this.padre = padre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Socket getSocket() {
		return socketCliente;
	}

	public boolean verificaNombre(String respuesta) {
		if (nombre.equals("") && !respuesta.equals("")) {
			boolean encontrado = false;
			for (Servidor_Escuchador cliente : padre.getEscuchadores()) {
				if (cliente.getNombre().equals(respuesta))
					encontrado = true;
			}
			if (!encontrado) {
				nombre = respuesta;
				return true;
			}
		}
		return false;
	}

	// no uso un switch para que sea compatible con java 1.6
	/*
	 * solo devuelve el true en los de cerrar, por que así puedo hacer el break
	 * en el otro lado.
	 */
	public boolean comandos(String mensaje) throws IOException {
		String cadena[] = mensaje.split("\\ ");
		try {
			if (cadena[0].equalsIgnoreCase("/help")) {
				help();
			} else if (cadena[0].equalsIgnoreCase("/time")) {
				padre.getEscritor().escribe(this, padre.getHoraActual());
			} else if (cadena[0].equalsIgnoreCase("/salir")) {
				socketCliente.close();
				padre.getEscuchadores().remove(this);
				return true;
			} else if (cadena[0].equalsIgnoreCase("/conectados")) {
				padre.getEscritor().escribe(
						this,
						"Tenemos " + padre.getEscuchadores().size()
								+ " Usuarios Conectados.");
			} else if (cadena[0].equalsIgnoreCase("/root")
					&& !padre.getAdministradores().contains(this)
					&& mensaje.equalsIgnoreCase(padre.getPwd())) {
				padre.getAdministradores().add(this);
			} else if (cadena[0].equalsIgnoreCase("/expulsar")
					&& padre.getAdministradores().contains(this)) {
				expulsar(cadena[1]);
			} else if (cadena[0].equalsIgnoreCase("/nombre")
					&& padre.getAdministradores().contains(this)) {
				cambiarNombre(cadena[1], cadena[2]);
			} else if (cadena[0].equalsIgnoreCase("/cerrarServer")
					&& padre.getAdministradores().contains(this)) {

				for(Servidor_Escuchador cliente:padre.getEscuchadores())
					padre.getEscritor().escribeConfiguracion(cliente,"/salir");
				
				System.exit(0);
				return true;
			} else {
				// esto es que si no es un comando, lo que dice el cliente se
				// envia
				// a los demas usuarios,
				// asi queda mas tipo chat.
				padre.getEscritor().escribeGeneral(this, mensaje);
			}
		} catch (IndexOutOfBoundsException error) {
			padre.getEscritor().escribe(this,
					"Comando mal escrito. Puedes consultar /help");
		}
		return false;
	}

	public void cambiarNombre(String nombre, String nuevoNombre)
			throws IOException {
		boolean encontrado = false;
		for (Servidor_Escuchador cliente : padre.getEscuchadores()) {
			if (cliente.getNombre().equals(nombre)) {
				padre.getEscritor().escribeGeneral(
						this,
						"Ha cambiado el nombre del cliente "
								+ cliente.getNombre() + ", ahora su nombre es "
								+ nuevoNombre);
				cliente.setNombre(nuevoNombre);
				encontrado = true;
				break;
			}
		}

		if (!encontrado)
			padre.getEscritor().escribe(this,
					"El nombre del usuario no existe o esta mal escrito.");
	}

	public void expulsar(String expulsado) throws IOException {
		boolean encontrado = false;
		for (Servidor_Escuchador cliente : padre.getEscuchadores()) {
			if (cliente.getNombre().equals(expulsado)) {
				padre.getEscritor().escribeConfiguracion(cliente, "/quit");
				cliente.getSocket().close();
				padre.getEscuchadores().remove(cliente);
				padre.getEscritor().escribeGeneral(this,
						"Ha expulsado al cliente " + cliente.getNombre());
				encontrado = true;
				break;
			}
		}

		if (!encontrado)
			padre.getEscritor().escribe(this,
					"El nombre del usuario no existe o esta mal escrito.");
	}

	public void help() throws IOException {
		String mensaje = "";
		/**
		 * Si eres un administrador puedes ver mas comandos que un simple
		 * usuario.
		 */
		if (padre.getAdministradores().contains(this))
			mensaje = "Ayuda del chat de javi: \n /salir    Para salir del chat.\n /help"
					+ "    Para mostrar los comandos del chat.\n /time    Para que el servidor te diga su hora.\n"
					+ " /privado [nombreOtroUsuario]    Para enviar un mensaje privado a otro usuario.\n"
					+ " /cerrarServer    Cierras el servidor y expulsas a todos los usuarios.\n"
					+ " /nombre [usuario] [nuevoNombre]    Cambias el nombre de un usuario o tu mismo.\n"
					+ " /expulsar [usuario]    Expulsarás del chat al usuario que escribas.\n";
		else
			mensaje = "Ayuda del chat de javi: \n /salir    Para salir del chat\n /help"
					+ "    Para mostrar los comandos del chat\n /time    para que el servidor te diga su hora\n"
					+ " /privado [nombreOtroUsuario]    para enviar un mensaje privado a otro usuario.\n"
					+ " /root [contraseña]    Convertirse en administrador.\n";

		padre.getEscritor().escribe(this, mensaje);

	}

	@Override
	public void run() {
		try {
			String fromclient;
			BufferedReader inFromClient;

			// Sincronizo un horario del cliente con el mio, el servidor.
			padre.getEscritor().escribeConfiguracion(this,
					String.valueOf(padre.getHoraServer().getTimeInMillis()));
			// fin sincronización.

			inFromClient = new BufferedReader(new InputStreamReader(
					socketCliente.getInputStream()));
			padre.getEscritor().escribe(this, "Cual es tu nombre?");
			while (nombre.equals("")) {
				try {
					fromclient = inFromClient.readLine();
					if (!verificaNombre(fromclient))
						padre.getEscritor()
								.escribe(this,
										"Nombre incorrecto o en uso, Cual es tu nombre?");
					else
						padre.getEscritor().escribe(this,
								"logueado correctamente como " + nombre);
				} catch (SocketException error) {
					System.err.println("Se ha forzado el cierre del cliente "
							+ nombre);
					socketCliente.close();
					padre.getEscuchadores().remove(this);
					break;
				}
			}

			while (true) {
				try {
					fromclient = inFromClient.readLine();
					if (comandos(fromclient))
						break;
					System.out.println(nombre + " > " + fromclient);
				} catch (SocketException error) {
					System.err.println("Se ha forzado el cierre del cliente "
							+ nombre);
					socketCliente.close();
					padre.getEscuchadores().remove(this);
					break;
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
