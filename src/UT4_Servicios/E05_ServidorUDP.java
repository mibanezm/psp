package UT4_Servicios;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.HashMap;

public class E05_ServidorUDP {
	private DatagramSocket socketServidor;
	private DatagramPacket in;
	private DatagramPacket out;
	private int puerto;
	private HashMap<InetSocketAddress, Cliente> clientes = new HashMap<InetSocketAddress, Cliente>();

	public E05_ServidorUDP(int puerto) {
		this.puerto = puerto;
	}

	public class Cliente extends Thread {
		private String mensaje;
		private InetSocketAddress isa;

		public Cliente(InetSocketAddress isa) {
			this.isa = isa;
		}

		@Override
		public void run() {
			synchronized (this) {
				try {
					enviarMensaje("Conexión Ok");
					do {
						wait();
						System.out.printf("cliente %s:%d>%s\n",
								isa.getHostName(), isa.getPort(), mensaje);
						if (mensaje.equals("adiós"))
							enviarMensaje("adiós");
					} while (!mensaje.equals("adiós"));
				} catch (Exception e) {

				} finally {
					clientes.remove(isa);
				}
			}
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}

		public void enviarMensaje(String msg) {
			out = new DatagramPacket(msg.getBytes(), msg.getBytes().length,
					isa.getAddress(), isa.getPort());
			try {
				socketServidor.send(out);
				System.out.printf("servidor para %s:%d>%s\n",
						isa.getHostName(), isa.getPort(), msg);
			} catch (IOException e) {
				System.err.println("Error enviando datos");
			}
		}
	}

	public void run() {
		try {
			socketServidor = new DatagramSocket(puerto);

			byte[] buffer = new byte[65536];
			in = new DatagramPacket(buffer, buffer.length);

			System.out.println("Esperando conexión entrante");
			Cliente c;
			while (true) {
				try {
					String mensaje = recibirMensaje();
					InetSocketAddress auxiliar = new InetSocketAddress(
							in.getAddress(), in.getPort());
					c = clientes.get(auxiliar);
					if (c == null) {
						c = new Cliente(auxiliar);
						clientes.put(auxiliar, c);
						c.start();
					} else {
						c = clientes.get(auxiliar);
						synchronized (c) {
							c.setMensaje(mensaje);
							c.notifyAll();
						}
					}
				} catch (IOException ioe) {
				}
			}
		} catch (IOException ioe) {
			System.err.printf(
					"No se puede crear el servidor en el puerto %d\n", puerto);
			System.exit(1);
		}
	}

	public String recibirMensaje() throws IOException {
		try {
			socketServidor.receive(in);
			return new String(in.getData(), 0, in.getLength());
		} catch (IOException e) {
			System.err.println("Error recibiendo datos");
			throw new IOException();
		}
	}

	public static void main(String args[]) {
		try {
			E05_ServidorUDP servidor = new E05_ServidorUDP(Integer.parseInt(args[0]));
			servidor.run();
		} catch (NumberFormatException nfe) {
			System.err
					.println("El primer argumento tiene que ser un número entero");
			System.exit(1);
		} catch (IndexOutOfBoundsException ioobe) {
			System.err.println("Hay que introducir el puerto como argumento");
			System.exit(1);
		}
	}
}
