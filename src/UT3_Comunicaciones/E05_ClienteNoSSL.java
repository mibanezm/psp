package UT3_Comunicaciones;

import javax.net.*; 
import java.net.*;
import java.io.*;

public class E05_ClienteNoSSL {
	public static void main (String args[]) throws IOException {

		int serverPort = 3000;

		if (args.length < 1) {
			System.out.println("java factorySocketClient serverHost serverPort");
			System.out.println("serverPort defaults to 3000 if not specified.");
			return;
		}
		if (args.length == 2)
			serverPort = new Integer(args[1]).intValue();

		System.out.println("Connecting to host " + args[0] + " at port " +
				serverPort);

		SocketFactory socketFactory = SocketFactory.getDefault();

		Socket  s = socketFactory.createSocket(args[0], serverPort);
		
		s.close();

		// Resto del programa
	}
}
