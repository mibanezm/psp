package examenJunio2013;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/* Cliente: envia un comando al servidor
 * 			un hilo auxiliar queda a la escucha de lo que contesta el servidor
 * Nota: la salida por pantalla no está sincronizada, podría hacerse con sincronización
 * por bloques y un objeto semáforo
 */
public class Cliente {

	//propiedades de Clente
	static final String S="Cliente: ";	//Para imprimir antes de cada mensaje 

	private Socket socketServicio;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private InetSocketAddress isa;

	//subclase ServidorRecibir: para crear hilo que procesa lo que se recibe del servidor
	public static class ServidorRecibir extends Thread {
		private Socket s=null;
		private ObjectOutputStream out=null;
		private ObjectInputStream in=null;
		private String mensaje=null;
		private boolean terminar=false;

		public ServidorRecibir(Socket s, ObjectInputStream in, ObjectOutputStream out) {
			this.s = s;
			this.in=in;
			this.out=out;
		}

		public void run() {
			this.setName(S+"Hilo ServidorRecibir");
			try {
				do {
					mensaje=recibirMensaje();
					System.out.printf(S+"mensaje desde [%s;%d]=%s\n", s.getInetAddress().getHostName(), s.getPort(),mensaje);
				} while (!terminar);
			} catch (IOException e) {
				System.err.println(S+"Conexión perdida");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				//cerrar flujos y conexión
				try {
					if (s != null) {
						in.close();
						out.close();
						s.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		String recibirMensaje() throws Exception {
			String msg=null;
			msg=(String)in.readObject();
			return msg;
		}
	}


	//Constructores de Cliente
	public Cliente(String host, int puerto) {
		isa = new InetSocketAddress(host, puerto);
	}

	//Métodos de Cliente
	void ejecutar() {
		String mensaje;
		//Hilo encargado de recibir respuestas desde el servidor
		try {
			Thread sr=null;
			socketServicio = new Socket(isa.getHostName(), isa.getPort());

			//Crear los flujos de E/S necesarios para comunicar con el servidor
			try {
				out = new ObjectOutputStream(socketServicio.getOutputStream());
				out.flush();
			} catch (IOException ioe) {
				System.err.println(S+"Error en la salida hacia el servidor");
				throw new IOException();
			}
			try {
				in = new ObjectInputStream(socketServicio.getInputStream());
			} catch (IOException ioe) {
				System.err.println(S+"Error en la entrada desde el servidor");
				throw new IOException();
			}

			(sr=new ServidorRecibir(socketServicio,in,out)).start();

			//Establecer con el conexión que se estará ejecutando previamente

			System.out.printf(S+"Conectado a [%s;%d]\n", isa.getHostName(),isa.getPort());

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			mensaje=null;
			try {
				//Leer comando desde entrada estandard y mandar al servidor
				System.out.println("Uso: [nº] mensaje\n\n");	//indicación de uso al operador
				do {
					System.out.print(S+"> ");		//símbolo de petición
					mensaje=br.readLine();
					enviarMensaje(mensaje);
				} while (!mensaje.equals("bye"));
			} catch (IOException ioException) {
				ioException.printStackTrace();
			} finally {

				//Terminar hilo receptor
				sr.interrupt();
				//cerrar flujos y conexión
				try {
					in.close();
					out.close();
					socketServicio.close();
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
			}

		} catch (ConnectException ce) {
			System.err.println(S+"Servidor caído");
			System.exit(1);
		} catch (UnknownHostException unknownHost) {
			System.err.printf(S+"Host desconocido: %s\n", isa.getHostName());
			System.exit(1);
		} catch (IOException ioException) {
			System.err.printf(S+"Excepción de E/S general\n");
			ioException.printStackTrace();
		}
	}

	void enviarMensaje(String msg) {
		try {
			out.writeObject(msg);
			out.flush();
			System.out.printf(S+"mensaje hacia [%s;%d]=%s\n", socketServicio.getInetAddress().getHostName(),
					socketServicio.getPort(), msg);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	public static void main(String args[]) {
		try {
			Cliente cliente = new Cliente(args[0],Integer.parseInt(args[1]));
			cliente.ejecutar();
		} catch (NumberFormatException nfe) {
			System.err.println(S+"Formato de número incorrecto");
			System.exit(1);
		} catch (IndexOutOfBoundsException ioobe) {
			System.err.println(S+"Hay que introducir dos argumentos: host y puerto");
			System.exit(1);
		}
	}
}
