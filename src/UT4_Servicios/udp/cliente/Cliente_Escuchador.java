package UT4_Servicios.udp.cliente;

import java.io.IOException;
import java.net.DatagramPacket;
public class Cliente_Escuchador implements Runnable {
	private UDP_Cliente padre;

	public Cliente_Escuchador(UDP_Cliente padre) {
		this.padre = padre;
	}

	@Override
	public void run() {
		try {
			while (true) {
				
				
				byte[] buffer = new byte[65536];
				DatagramPacket respuesta = new DatagramPacket(buffer,
						buffer.length);
				padre.getSock().receive(respuesta);

				byte[] dataRespuesta = respuesta.getData();
				String mensaje = new String(dataRespuesta, 0,
						respuesta.getLength());
				System.out.println(mensaje);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
