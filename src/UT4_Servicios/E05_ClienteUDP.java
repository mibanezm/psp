package UT4_Servicios;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class E05_ClienteUDP {
	private DatagramSocket socketServicio;
	private DatagramPacket in;
	private DatagramPacket out;
	private String mensaje;
	private InetSocketAddress isa;

	public E05_ClienteUDP(String host, int puerto) {
		isa = new InetSocketAddress(host, puerto);
	}

	public void run() {
		try {
			try {
				socketServicio = new DatagramSocket();
				socketServicio.connect(isa);
			} catch (SocketException e) {
				System.err.println("Error de conexión");
				throw new SocketException();
			}

			byte[] buffer = new byte[65536];
			in = new DatagramPacket(buffer, buffer.length);

			enviarMensaje("Hi");
			System.out.println("Esperando respuesta...");
			do {
				mensaje = recibirMensaje();
				System.out.println("servidor>" + mensaje);
				enviarMensaje("Hola, servidor");
				mensaje = "adiós";
				enviarMensaje(mensaje);
			} while (!mensaje.equals("adiós"));
		} catch (Exception e) {
			System.exit(1);
		} finally {
			if (socketServicio != null)
				socketServicio.close();
		}
	}

	public String recibirMensaje() throws IOException {
		try {
			socketServicio.receive(in);
			return new String(in.getData(), 0, in.getLength());
		} catch (IOException e) {
			System.err.println("El servidor no está conectado");
			throw new IOException();
		}
	}

	public void enviarMensaje(String msg) {
		byte[] b = msg.getBytes();
		out = new DatagramPacket(b, b.length);
		try {
			socketServicio.send(out);
			System.out.println("cliente>" + msg);
		} catch (IOException e) {
			System.err.println("Error enviando datos");
		}
	}

	public static void main(String args[]) {
		try {
			E05_ClienteUDP cliente = new E05_ClienteUDP(args[0],
					Integer.parseInt(args[1]));
			cliente.run();
		} catch (NumberFormatException nfe) {
			System.err
					.println("El segundo argumento tiene que ser un número entero");
			System.exit(1);
		} catch (IndexOutOfBoundsException ioobe) {
			System.err
					.println("Hay que introducir dos argumentos: host y puerto");
			System.exit(1);
		}
	}
}
